<?php
namespace Application\Controller;

use Application\Service\ContactApiService;
use Application\Repository\ContactRepository;


class ContactApiController
{

    public $sm;

    public function __construct($sm)
    {
        $this->sm = $sm;
    }

    public function  contactsAction()
    {
        $contactRep = new ContactRepository($this->sm);
        $contacts = $contactRep->getContacts();

        echo json_encode($contacts); //ha sok elem van, akkor  Traversable PDO-val
    }

    public function  contactAction()
    {
        $get = $this->sm->filterRecursive($_GET);
        $id = explode('/', $get['q'])[1];
        $contactRep = new ContactRepository($this->sm);
        $contact = $contactRep->getContact($id);

        echo json_encode($contact);

    }

    public function  contactmodifyAction()
    {

        if ($_SERVER['REQUEST_METHOD'] !== 'PUT') return;

        try {

            $_PUT = file_get_contents("php://input");

            $_PUT = json_decode($_PUT, true);
            if (!is_array($_PUT)) throw new \Exception('Error: invalid JSON data');

            $datas = $this->sm->filterRecursive($_PUT);


            $get = $this->sm->filterRecursive($_GET);
            $id = explode('/', $get['q'])[1];

            $contactSer = new ContactApiService();
            $contactSer->contactDataValidator($datas);
            $contactSer->setRepo(new ContactRepository($this->sm));

            echo($contactSer->modifyContact($id, $datas) ? 'Success' : 'Error');

        } catch (\Exception $e) {
            echo $e->getMessage();
        }


    }

    public function  contactcreateAction()
    {

        if ($_SERVER['REQUEST_METHOD'] !== 'PUT') return;

        try {
            $_PUT = file_get_contents("php://input");

            $_PUT = json_decode($_PUT, true);
            if (!is_array($_PUT)) throw new \Exception('Error: invalid JSON data');

            $datas = $this->sm->filterRecursive($_PUT);

            $contactSer = new ContactApiService();
            $contactSer->contactNewDataValidator($datas);
            $repo = new ContactRepository($this->sm);

            echo($repo->createContact($datas) ? 'Success' : 'Error');

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }


}