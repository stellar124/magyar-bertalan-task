<?php
namespace Application\Controller;
//namespace autoloader stb.

class IndexController
{

    public $sm;

    public function __construct($sm)
    {
        $this->sm = $sm;
    }

    public function  indexAction()
    {


        echo '
            Get all contacts: GET /contacts <br>
            Get a contact: GET /contacts/{id} <br>
            Create a contact: PUT /contactcreate BODY: {"name":"Teszt Elek","email":"teszt@elek.com","phone_number":"+3630123456","address":"1111 Budapest Kossuth Lajos utca 44/a"} <br>
            Modify a contact: PUT /contactmodify/3 BODY: {"name":"Teszt Elek","email":"teszt@elek.com","phone_number":"+3630123456","address":"1111 Budapest Kossuth Lajos 44/a"}<br><br>';

        echo 'Local config location: config/config_local.php <br><br>';
        echo 'SQL: <pre>';
        echo "

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `phone_number`, `address`) VALUES
(1, 'Gipsz Jakab', 'gipszj@gg.com', '+361234567', '9999 Gubafalva Kossuth Lajos utca 2.'),
(2, '', '', '', ''),
(3, 'Teszt Elek', 'teszt@elek.com', '+3698765432', '8888 Surmotanya Kossuth Lajos utca 33.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

";
        print '</pre>';
    }


}