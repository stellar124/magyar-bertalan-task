<?php
namespace Application\Service;


class ContactApiService
{

    private $repo;

    public function setRepo($repo)
    {
        $this->repo = $repo;
    }


    /**
     * @param array $datas
     * @return bool
     * @throws \Exception
     */
    public function contactDataValidator(array $datas): bool
    {
        if (isset($datas['email']) && preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i', $datas['email']) !== 1) throw new \Exception('Error: not valid email!');
        if (isset($datas['phone_number']) && preg_match('/^[0-9\+\-\(\)\s]{4,15}$/', $datas['phone_number']) !== 1) throw new \Exception('Error: not valid phone number!');
        if (isset($datas['name']) && strlen($datas['name']) > 255 || strlen($datas['name']) < 5) throw new \Exception('Error: too long/short name!');
        if (isset($datas['address']) && strlen($datas['address']) > 255 || strlen($datas['address']) < 5) throw new \Exception('Error: too long/short address!');

        return true;
    }

    /**
     * @param array $datas
     * @return bool
     * @throws \Exception
     */
    public function contactNewDataValidator(array $datas) : bool
    {
        if (!isset($datas) ||
            !isset($datas['email']) ||
            !isset($datas['phone_number']) ||
            !isset($datas['name']) ||
            !isset($datas['address'])
        ) {
            throw new \Exception('Error: All fields are reqiured');
        }
        return $this->contactDataValidator($datas);
    }

    /**
     * @param string $id
     * @param array $datas
     * @return mixed
     * @throws \Exception
     */
    public function modifyContact(string $id, array $datas) :bool
    {
        $current = $this->repo->getContact($id);
        if (empty($current)) throw new \Exception('Error: No such contact!');

        $merged = array_merge($current, $datas);

        return $this->repo->modifyContact($id, $merged);

    }


}