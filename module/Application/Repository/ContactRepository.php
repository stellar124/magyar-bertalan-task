<?php

namespace Application\Repository;

use CommonService\CommonService;

class ContactRepository
{

    private $db;

    public function __construct(CommonService $sm)
    {
        $this->db = $sm->getDb();
    }


    /**
     * @return mixed
     * @throws \Exception
     */
    public function getContacts()
    {
        try {
            $stmt = $this->db->prepare('SELECT * FROM contact');
            $stmt->execute();

            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $pe) {
            throw $pe;
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function getContact(string $id)
    {
        try {
            $stmt = $this->db->prepare('SELECT * FROM contact WHERE id = :id');
            $stmt->bindParam(':id', $id);
            $stmt->execute();

            return $stmt->fetch(\PDO::FETCH_ASSOC);
        } catch (\PDOException $pe) {
            throw $pe;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $datas
     * @return bool
     * @throws \Exception
     */
    public function modifyContact(string $id, array $datas) : bool
    {

        try {
            $stmt = $this->db->prepare("UPDATE contact set name = :name, email = :email, phone_number = :phone_number, address = :address WHERE id = :id;");
            $stmt->bindParam(':name', $datas['name']);
            $stmt->bindParam(':email', $datas['email']);
            $stmt->bindParam(':phone_number', $datas['phone_number']);
            $stmt->bindParam(':address', $datas['address']);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
        } catch (\PDOException $pe) {
            throw $pe;
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }

    /**
     * @param array $datas
     * @return bool
     * @throws \Exception
     */
    public function createContact(array $datas) : bool
    {

        try {
            $stmt = $this->db->prepare("INSERT INTO contact (name, email, phone_number, address) VALUES (:name, :email, :phone_number, :address)");
            $stmt->bindParam(':name', $datas['name']);
            $stmt->bindParam(':email', $datas['email']);
            $stmt->bindParam(':phone_number', $datas['phone_number']);
            $stmt->bindParam(':address', $datas['address']);
            $stmt->execute();
        } catch (\PDOException $pe) {
            throw $pe;
        } catch (\Exception $e) {
            throw $e;
        }

        return true;


    }


}