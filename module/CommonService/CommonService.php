<?php
namespace CommonService;
/**
 * Common service for all modules
 */
class CommonService 
{
    /**
     * get PDO instance
     *
     * @return PDO
     */
    public function getDb() : \PDO
    {
        try {
            $conn = new \PDO('mysql:host=' . MYSQL_HOST . ';dbname=' . MYSQL_DBNAME . ';charset=utf8', MYSQL_DBUSER, MYSQL_DBPW);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $conn;

		} catch (\Exception $e) {
			die('Database connection error');
		}
    }

    /**
     * Recursive user input filter
     *
     * @param array $dirty
     * @return array
     */
    public function filterRecursive(array $dirty) : array
    {
        $helper = [];

        if (!is_array($dirty)) return [];

        foreach ($dirty as $key => $value) {
            $key = strip_tags(substr($key, 0, 32 ));
            $helper[$key] = is_array($value)
                ? filterRecursive($value)
                : (is_string($value) ? strip_tags(substr($value, 0, 2048 )) : ((is_numeric($value) || is_bool($value)) ? $value : ""));
        }
        //stb filter technológiák

        return $helper;
    }

}