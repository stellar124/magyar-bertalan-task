<?php

declare(strict_types=1);

require_once('config_local.php');

//Class Autoloader
spl_autoload_register(function ($className) {

    $className = str_replace('\\','/',$className);

    $path = __DIR__ . "/../module/" .  $className . '.php';

    if (file_exists($path)) {
        require_once($path);
    } else {
        die("The file {$className}.php could not be found.");
    }
});


function route(string $route) {

    if($route === ''){
        $controllerName = '\\Application\\Controller\\IndexController';
        $actionName = 'indexAction';

        return [$controllerName, $actionName];
    }

    switch($route) {
        case (preg_match('/^contacts$/', $route) === 1 ? $route : ''):
            $controllerName = '\\Application\\Controller\\ContactApiController';
            $actionName = 'contactsAction';

            return [$controllerName, $actionName];

        case (preg_match('/^contacts\/[0-9]{1,11}$/', $route) === 1 ? $route : ''):
            $controllerName = '\\Application\\Controller\\ContactApiController';
            $actionName = 'contactAction';

            return [$controllerName, $actionName ];

        case (preg_match('/^contactmodify\/[0-9]{1,11}$/', $route) === 1 ? $route : ''):
            $controllerName = '\\Application\\Controller\\ContactApiController';
            $actionName = 'contactmodifyAction';

            return [$controllerName, $actionName ];

        case (preg_match('/^contactcreate$/', $route) === 1 ? $route : ''):
            $controllerName = '\\Application\\Controller\\ContactApiController';
            $actionName = 'contactcreateAction';

            return [$controllerName, $actionName ];

        default:
            $controllerName = '\\Application\\Controller\\IndexController';
            $actionName = 'indexAction';
            return [$controllerName, $actionName ];

    }

}