<?php

require_once('../config/config.php');

$get =
$q = $_GET['q'] ?? '';

$route = route($q);
$controller = $route[0];
$action = $route[1];

// Call the action
$load = new $controller(new CommonService\CommonService());
$load->$action();